"use strict";
function init() {
  drawAllPointsCircle();
  // вызови функцию loadCatalog для загрузки первой страницы каталога
  let currentPage = 1;
  loadCatalog(currentPage);
  // Реализуй и установи обработчик нажатия на кнопку "Загрузить еще"
  const loadMoreBtn = getLoadMoreBtn();

  loadMoreBtn.addEventListener("click", () => {
    currentPage++;
    loadCatalog(currentPage);
  });
}

function drawAllPointsCircle() {
  const curPointId = getPointId();
  const allPointsElem = document.getElementById("pick-points__list__all");

  if (curPointId) {
    allPointsElem.classList.remove("active");
  } else {
    allPointsElem.classList.add("active");
  }
}

function getLoadMoreBtn() {
  return document.getElementById("loadMore");
}

async function loadCatalog(page) {
  // Здесь необходимо сделать загрузку каталога (api.getBikes)
  // и передать полученные данные в функции appendCatalog и showButtonLoadMore
  const pointId = getPointId();
  disableButtonLoadMore();
  getBikes(pointId, page)
    .then((bikes) => {
      appendCatalog(bikes.bikesList);
      enableButtonLoadMore();
      showButtonLoadMore(bikes.hasMore);
    })
    .catch((error) => console.log(error));
}

function getBikes(pointId, page) {
  let options = {
    method: "GET",
  };
  return fetch(`/api/catalog/${pointId}?page=${page}`, options)
    .then((response) => response.json())
    .catch((error) => console.log(error));
}

function appendCatalog(items) {
  // отрисуй велосипеды из items в блоке <div id="bikeList">
  const bikeListBlock = document.getElementById("bikeList");

  items.forEach((item) => {
    const bikeBlock = getBikeBlock();
    bikeListBlock.appendChild(bikeBlock);

    const bikeBlockTop = getBikeBlockTop();
    bikeBlock.appendChild(bikeBlockTop);

    const bikeBlockBottom = getBikeBlockBottom();
    bikeBlock.appendChild(bikeBlockBottom);

    const bikeImgBlock = getImgBlock();
    bikeBlockTop.appendChild(bikeImgBlock);

    const bikeImg = getImg(item);
    bikeImgBlock.appendChild(bikeImg);

    const bikeTitle = getBikeTitle(item);
    bikeBlockTop.appendChild(bikeTitle);

    const bikeCost = getBikeCost(item);
    bikeBlockBottom.appendChild(bikeCost);

    const bikeRentBtn = getRentBtn(item);
    bikeBlockBottom.appendChild(bikeRentBtn);
  });
}

function getBikeBlock() {
  const bikeBlock = document.createElement("div");
  bikeBlock.className = "bike-block";

  return bikeBlock;
}

function getBikeBlockTop() {
  const bikeBlockTop = document.createElement("div");
  bikeBlockTop.className = "bike-block__top";

  return bikeBlockTop;
}

function getBikeBlockBottom() {
  const bikeBlockBottom = document.createElement("div");
  bikeBlockBottom.className = "bike-block__bottom";

  return bikeBlockBottom;
}

function getImgBlock() {
  const bikeImgBlock = document.createElement("div");
  bikeImgBlock.className = "bike-img-block";

  return bikeImgBlock;
}

function getImg(item) {
  const bikeImg = document.createElement("img");
  bikeImg.src = `/images/${item.img}`;
  bikeImg.alt = "bike image";

  return bikeImg;
}

function getBikeTitle(item) {
  const bikeTitle = document.createElement("div");
  bikeTitle.className = "bike-block__title";
  bikeTitle.textContent = item.name;

  return bikeTitle;
}

function getBikeCost(item) {
  const bikeCost = document.createElement("div");
  bikeCost.className = "bike-block__cost";
  bikeCost.textContent = `Стоимость за час - ${item.cost * 60}`;

  return bikeCost;
}

function getRentBtn(item) {
  const bikeRentBtn = document.createElement("button");
  bikeRentBtn.className = "bike-block__rent btn";
  bikeRentBtn.textContent = "Арендовать";

  bikeRentBtn.addEventListener("click", () => {
    document.location.href = `/order/${item._id}`;
  });

  return bikeRentBtn;
}

function showButtonLoadMore(hasMore) {
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
  const buttonBlock = getLoadMoreBtn();

  if (hasMore) {
    buttonBlock.classList.remove("hidden");
  } else {
    buttonBlock.classList.add("hidden");
  }
}

function disableButtonLoadMore() {
  // заблокируй кнопку "загрузить еще"
  const loadMoreBtn = getLoadMoreBtn();
  loadMoreBtn.disabled = true;
}

function enableButtonLoadMore() {
  // разблокируй кнопку "загрузить еще"
  const loadMoreBtn = getLoadMoreBtn();
  loadMoreBtn.disabled = false;
}

function getPointId() {
  // сделай определение id выбранного пункта проката
  const path = document.location.pathname;
  let idInPath = path.split("/").pop();

  if (idInPath === "catalog") {
    idInPath = "";
  }

  return idInPath;
}

document.addEventListener("DOMContentLoaded", init);
