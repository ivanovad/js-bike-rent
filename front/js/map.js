ymaps.ready(init);
async function init() {
  let pointersMap = new ymaps.Map("map", {
    center: [55.01, 82.9],
    zoom: 13,
  });

  let myCollection = new ymaps.GeoObjectCollection({});

  const pointers = await api.getPointers();

  pointers.forEach((point) => {
    myCollection.add(
      new ymaps.Placemark(point.coordinates, {
        balloonContentBody: `
        <div class="popup">
          <div class="popup__text">
            Пункт по адресу ${point.address}
          </div>
          <button class="popup_btn btn" onclick="window.location.href='/catalog/${point._id}'">
            Выбрать велосипед
          </button>
        <div>`,
      })
    );
  });

  pointersMap.geoObjects.add(myCollection);
  pointersMap.setBounds(myCollection.getBounds());
}
