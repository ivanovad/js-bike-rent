const formElem = document.getElementById("card-form");
const allInputElems = document.querySelectorAll("input");

formElem.addEventListener("submit", (event) => {
  event.preventDefault();

  const countOfFields = 3;
  let successValidCount = 0;

  allInputElems.forEach((inputElem) => {
    if (inputValidation(inputElem)) {
      successValidCount += 1;
    }
  });

  const requisites = {
    date: getDateValue(),
    number: getNumberValue(),
    cvv: getCvvValue(),
  };

  if (successValidCount === countOfFields) {
    saveCardRequisites(requisites);
  }
});

function getNumberValue() {
  const cardNumberElem = document.getElementById("card-form__number");
  return cardNumberElem.value;
}

function getDateValue() {
  const dateElem = document.getElementById("card-form__expiry");
  return dateElem.value;
}

function getCvvValue() {
  const cvvElem = document.getElementById("card-form__cvv");
  return cvvElem.value;
}

function saveCardRequisites(requisites) {
  let options = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(requisites),
    credentials: "include",
  };

  return fetch("/api/card-requisites", options)
    .then((response) => {
      if (response.ok) {
        return response;
      } else {
        throw new Error(
          `Запрос завершился не успешно: ${response.status}: ${response.statusText}`
        );
      }
    })
    .then(() => {
      let queryParams = window.location.search.split("%2F");
      console.log(queryParams);
      if (queryParams.length === 3) {
        let bikeId = queryParams[2];

        location.replace(`/order/${bikeId}`);
      } else {
        location.replace("/lk");
      }
    })
    .catch((error) => console.log(error));
}

allInputElems.forEach((inputElem) => {
  inputElem.addEventListener("change", () => {
    inputValidation(inputElem);
  });
});

function inputValidation(input) {
  const noSpaceText = getNoSpaceText(input.value);

  hideError(input);
  if (isEmptyInput(noSpaceText)) {
    renderError(input, "Поле не может быть пустым");
    return false;
  }
  if (!isNumber(noSpaceText) && input.id !== "card-form__expiry") {
    renderError(input, "Поле может содержать только цифры");
    return false;
  }

  switch (input.id) {
    case "card-form__number":
      if (!cardNumberValidate(input, noSpaceText)) {
        return false;
      }
      break;
    case "card-form__expiry":
      if (!cardDateValidation(input, noSpaceText)) {
        return false;
      }
      break;
    case "card-form__cvv":
      if (!cvvValidate(input, noSpaceText)) {
        return false;
      }
      break;
    default:
      break;
  }

  return true;
}

function isEmptyInput(string) {
  return string ? false : true;
}

function getNoSpaceText(string) {
  return string.replace(/\s+/g, "");
}

function isNumber(elem) {
  return Number.isInteger(Number(elem));
}

function renderError(elem, error) {
  const tooltipElem = getTooltipElem(elem);

  tooltipElem.classList.add("visible");
  tooltipElem.textContent = error;
  elem.style.borderColor = "#E30000";
}

function getTooltipElem(elem) {
  const tooltipId = getTooltipId(elem.id);
  const tooltipElem = document.getElementById(tooltipId);

  return tooltipElem;
}

function getTooltipId(inputId) {
  return inputId + "__tooltip";
}

function hideError(elem) {
  const tooltipElem = getTooltipElem(elem);

  tooltipElem.classList.remove("visible");
  elem.style.borderColor = "#CECECE";
}

function cardNumberValidate(cardNumberElem, cardNumberValue) {
  if (!isCorrectLength(cardNumberValue)) {
    renderError(cardNumberElem, "Номер карты должен состоять из 16 цифр");
    return false;
  }
  const formattedCardNumber = getFormattedCardNumber(cardNumberValue);

  setCardValue(cardNumberElem, formattedCardNumber);
  return true;
}

function isCorrectLength(cardNumberValue) {
  const cardLength = 16;
  return cardNumberValue.length === cardLength;
}

function getFormattedCardNumber(cardNumberValue) {
  const symbolsToSplit = 4;
  let formattedCardNumber = "";
  for (let i = 0; i < cardNumberValue.length; i += symbolsToSplit) {
    formattedCardNumber += `${cardNumberValue.substr(i, symbolsToSplit)} `;
  }

  return formattedCardNumber.slice(0, -1);
}

function setCardValue(elem, newValue) {
  elem.value = newValue;
}

function cardDateValidation(cardDateElem, cardDateValue) {
  const onlyNumbersValue = getOnlyNumbersValue(cardDateValue);

  if (!isNumber(onlyNumbersValue) || !isCorrectDateLength(onlyNumbersValue)) {
    renderError(
      cardDateElem,
      "Поле должно содержать цифры и разделитель в формате мм/гг"
    );

    return false;
  }

  const month = Number(onlyNumbersValue.slice(0, 2));
  const year = Number(onlyNumbersValue.slice(2, 4));
  const formattedCardDate = getFormattedCardDate(month, year);
  setCardValue(cardDateElem, formattedCardDate);

  if (!isCorrectMonth(month)) {
    renderError(cardDateElem, "Некорретный номер месяца");
    return false;
  }

  if (!isCorrectYear(year)) {
    renderError(cardDateElem, "Некорректный год");
    return false;
  }

  return true;
}

function getOnlyNumbersValue(value) {
  return value
    .split("")
    .filter((symbol) => isNumber(symbol))
    .join("");
}

function isCorrectDateLength(date) {
  return date.length === 4;
}

function isCorrectMonth(month) {
  return month >= 1 && month <= 12;
}

function isCorrectYear(year) {
  const curDate = new Date();
  const curYear = curDate.getFullYear();
  const lastTwoYearDigits = curYear % 100;
  const lastTwoCardEndDigits = (curYear + 5) % 100;

  return year >= lastTwoYearDigits && year <= lastTwoCardEndDigits;
}

function getFormattedCardDate(month, year) {
  let monthString = month < 10 ? "0" + String(month) : String(month);
  let yearString = year < 10 ? "0" + String(year) : String(year);

  return `${monthString}/${yearString}`;
}

function cvvValidate(cardCvvElem, cardCvvValue) {
  if (cardCvvValue.length !== 3) {
    renderError(cardCvvElem, "CVV код должен содержать три цифры");
    return false;
  }

  return true;
}
