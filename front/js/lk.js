const buttonReturnElems = document.querySelectorAll(".bike-block__return-lk");

buttonReturnElems.forEach((returnBtn) => {
  returnBtn.addEventListener("click", (event) => {
    const orderId = event.target.dataset.orderId;

    returnBikeOrder(orderId);
  });
});

function returnBikeOrder(orderId) {
  let options = {
    method: "DELETE",
  };
  const bikeContainer = document.querySelector(".lk-bike-container");

  return fetch(`/api/order/${orderId}`, options)
    .then(() => {
      const orderElem = document.getElementById(`${orderId}`);

      if (bikeContainer.contains(orderElem)) {
        orderElem.remove();
      }
      if (bikeContainer.children.length === 0) {
        location.reload();
      }
    })
    .catch((error) => console.log(error));
}

document.getElementById("change-card-data").addEventListener("click", () => {
  document.location.href = "card-requisites";
});
